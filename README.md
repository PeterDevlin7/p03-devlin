# README #

### What is this repository for? ###

* This repository stores the code necessary to run the Doodle Jump Clone created by Peter Devlin
* Version 1.0

### Code Recognition ###

* Being that I am still new to XCode and Objective-C, I am using **Professor Madden's** code skeleton as a basis for the game.

### Who do I talk to? ###

* Peter Devlin email: pdevlin1@binghamton.edu