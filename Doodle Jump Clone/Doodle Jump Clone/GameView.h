//
//  GameView.h
//  Doodle Jump Clone
//
//  Created by Peter Devlin on 2/14/17.
//  Copyright © 2017 Peter Devlin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Jumper.h"
#import "Brick.h"

@interface GameView : UIView

@property (nonatomic, strong) Jumper *jumper;
@property (nonatomic, strong) NSMutableArray *bricks;
@property (nonatomic) float tilt;
@property (nonatomic, strong) IBOutlet UIViewController *vc;
-(void)arrange:(CADisplayLink *)sender;
-(void)setDimensions:(float)height width:(float) width;
-(void)startBricks;
-(void)endGame;

@end
