//
//  Jumper.h
//  Doodle Jump Clone
//
//  Created by Peter Devlin on 2/14/17.
//  Copyright © 2017 Peter Devlin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Jumper : UIView

@property (nonatomic) float dx, dy;  // Velocity
@property (nonatomic) CGRect rect;   // Frame

@end
