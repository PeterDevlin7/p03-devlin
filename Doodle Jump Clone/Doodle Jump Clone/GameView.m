//
//  GameView.m
//  Doodle Jump Clone
//
//  Created by Peter Devlin on 2/14/17.
//  Copyright © 2017 Peter Devlin. All rights reserved.
//

#import "GameView.h"

@implementation GameView

@synthesize jumper, bricks;
@synthesize tilt;
@synthesize vc;
int numBricks = 7;
float oldTilt = 0.0;
float horizontalShift = 0.0;
float oldHorizontalShift = 0.0;
float boundaryMargin = 10.0;
float actualHeight = 0.0;
float actualWidth = 0.0;
float height = 11;
float prevHeight = 0.0;
float prevWidth = 0.0;

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    return self;
}

-(void)setDimensions:(float) height width:(float) width {
    actualHeight = height;
    actualWidth = width;
    NSLog(@"Actual height: %f\nActual width: %f", actualHeight, actualWidth);
}

-(void)startBricks
{
    
    jumper = [[Jumper alloc] initWithFrame:CGRectMake(actualWidth/2, actualHeight - 20, 20, 20)];
    [jumper setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"cat.png"]]];
    [jumper setDx:0];
    [jumper setDy:10];
    [self addSubview:jumper];
    
    if (bricks)
    {
        for (Brick *brick in bricks)
        {
            [brick removeFromSuperview];
        }
    }
    
    bricks = [[NSMutableArray alloc] init];
    Brick *floor = [[Brick alloc] initWithFrame:CGRectMake(0, 0, actualWidth, height)];
    [floor setBackgroundColor:[UIColor blackColor]];
    [floor setCenter:CGPointMake(actualWidth/2, actualHeight)];
    [self addSubview:floor];
    [bricks addObject:floor];
    prevHeight = actualHeight;
    for (int i = 1; i <= numBricks+1; ++i)
    {
        [self generateBrick:numBricks-i];
    }
    
}
/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

-(void)arrange:(CADisplayLink *)sender
{
    //CFTimeInterval ts = [sender timestamp];
    
    // Apply gravity to the acceleration of the jumper
    [jumper setDy:[jumper dy] - .3];
    
    // Apply the tilt.  Limit maximum tilt to + or - 5
    horizontalShift = jumper.dx + tilt;
    //NSLog(@"Tilt: %f",tilt);
    if(oldTilt == tilt)
        horizontalShift = oldHorizontalShift / 1.25;
    [jumper setDx:horizontalShift];
    if ([jumper dx] > 5)
        [jumper setDx:5];
    if ([jumper dx] < -5)
        [jumper setDx:-5];

    oldTilt = tilt;
    oldHorizontalShift = horizontalShift;
    // Jumper moves in the direction of gravity
    CGPoint p = [jumper center];
    p.x += [jumper dx];
    p.y -= [jumper dy];
    
    //If the jumper has moved above 1/3 of the screen and is moving up, start moving bricks down
    if(jumper.dy > 0 && p.y < actualHeight/3) {
        [self moveBricksDown];
        p.y += [jumper dy];
    }
    
    // If the jumper has fallen below the bottom of the screen,
    // end the game and start over
    if (p.y > actualHeight + 10)
    {
        [jumper setDy: 0];
        p.y += 20;
        [jumper setCenter:p];
        [self endGame];
        return;
    }
    
    // If we have gone too far left, or too far right, stop at the side
    if (p.x < boundaryMargin) {
        p.x = boundaryMargin;
    }
    if (p.x > actualWidth - boundaryMargin) {
        p.x = actualWidth - boundaryMargin;
    }
    
    CGPoint bottomLeft = CGPointMake(p.x - 10, p.y + 10);
    CGPoint bottomRight = CGPointMake(p.x + 10, p.y + 10);
    // If we are moving down, and we touch a brick, we get
    // a jump to push us up.
    if ([jumper dy] < 0)
    {
        for (Brick *brick in bricks)
        {
            CGRect b = [brick frame];
            if (CGRectContainsPoint(b, bottomLeft) || CGRectContainsPoint(b, bottomRight) || CGRectContainsPoint(b, p))
            {
                // Yay!  Bounce!
                NSLog(@"Bounce!");
                [jumper setDy:10];
            }
        }
    }
    
    [jumper setCenter:p];
    // NSLog(@"Timestamp %f", ts);
}

-(void)moveBricksDown {
    
    NSMutableArray *removeBricks;
    removeBricks = [[NSMutableArray alloc] init];
    for (Brick *brick in bricks) {
        CGPoint p = [brick center];
        p.y += jumper.dy;
        [brick setCenter:p];
        if(p.y > actualHeight) {
            [removeBricks addObject: brick];
        }
    }
    for (Brick *brick in removeBricks) {
        [bricks removeObject:brick];
        [brick removeFromSuperview];
        if(brick.frame.size.width < actualWidth) {
            [self generateBrick:-1];
        }
    }
}

-(void)generateBrick:(int)i {
    
    float width = actualWidth * .195;
    float heightPlacement = i*actualHeight/numBricks;
    float widthAdjustment = (100.0 - (arc4random_uniform(10)))/100.0;
    float heightVariance = (rand() % (int)(actualHeight/numBricks));
    //This makes sure bricks aren't too far apart
    while(heightPlacement + heightVariance - prevHeight > (1.4*actualHeight/numBricks)) {
        heightVariance /= 2;
    }
    prevHeight = heightPlacement + heightVariance;
    float widthPlacement = (rand() % (int)(actualWidth * .85));
    if(widthPlacement < actualWidth*.25 && prevWidth > actualWidth*.75) {
        widthPlacement += actualWidth*.35;
    }
    if(widthPlacement > actualWidth*.75 && prevWidth < actualWidth*.25) {
        widthPlacement -= actualWidth*.35;
    }
    prevWidth = widthPlacement;
    Brick *b = [[Brick alloc] initWithFrame:CGRectMake(0, 0, width * widthAdjustment, height)];
    //NSLog(@"Actual Width: %f", width * widthAdjustment);
    [b setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"platform.png"]]];
    [self addSubview:b];
    [b setCenter:CGPointMake(widthPlacement, prevHeight)];
    [bricks addObject:b];
}

-(void)endGame {
    NSString *scoreString = [NSString stringWithFormat:@"You lost!"];
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Game Over"
                                                                   message:scoreString
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [vc presentViewController:alert animated:YES completion:nil];
    [self startBricks];
}

@end
