//
//  main.m
//  Doodle Jump Clone
//
//  Created by Peter Devlin on 2/14/17.
//  Copyright © 2017 Peter Devlin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
