//
//  ViewController.m
//  Doodle Jump Clone
//
//  Created by Peter Devlin on 2/14/17.
//  Copyright © 2017 Peter Devlin. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()


@end

@implementation ViewController

@synthesize displayLink;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
}

-(void)viewDidAppear:(BOOL)animated {
    
    [_gameView setDimensions: self.view.frame.size.height width: self.view.frame.size.width];
    [_gameView startBricks];
    displayLink = [CADisplayLink displayLinkWithTarget:_gameView selector:@selector(arrange:)];
    [displayLink addToRunLoop:[NSRunLoop mainRunLoop] forMode:NSDefaultRunLoopMode];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)speedChange:(id)sender
{
    UIPanGestureRecognizer *s = (UIPanGestureRecognizer *)sender;
    // NSLog(@"tilt %f", (float)[s value]);
    [_gameView setTilt:(float)[s velocityInView:_gameView].x];
}

@end
